# Large Repo Generator

Designed to generate a large repo with random content quickly.

A few notes:

- The content is 100% random. It's not meant to make sense.
- It doesn't really make a repo. It really just populates directories.
- It will generate a 30GB repo unless you pass it a JSON file with some values for the variables.



To find out how much space your repo is taking:

```
du -sh
```


