# A simple script to generate a large Git repo
# Run it in the directory where you want the repo


import secrets
from pathlib import Path
import sys
import string
from datetime import datetime
import json

# Default values

if len(sys.argv) > 1:
    filename = sys.argv[1]
    jsoncode = Path(filename).read_text()
    values = json.loads(jsoncode)
    NUMBER_OF_CODE_FILES = values['NUMBER_OF_CODE_FILES']
    LINES_PER_CODE_FILE = values['LINES_PER_CODE_FILE']
    CHARACTERS_PER_LINE = values['CHARACTERS_PER_LINE']
    NUMBER_OF_LARGE_FILES = values['NUMBER_OF_LARGE_FILES']
    BYTES_PER_LARGE_FILE = values['BYTES_PER_LARGE_FILE']
    NUMBER_OF_SMALL_FILES = values['NUMBER_OF_SMALL_FILES']
    CHARACTERS_PER_SMALL_FILE = values['CHARACTERS_PER_SMALL_FILE']
else:
    NUMBER_OF_CODE_FILES = 1500
    LINES_PER_CODE_FILE = 500
    CHARACTERS_PER_LINE = 80
    NUMBER_OF_LARGE_FILES = 250
    BYTES_PER_LARGE_FILE = 10*1024*1024
    NUMBER_OF_SMALL_FILES = 1000
    CHARACTERS_PER_SMALL_FILE = 10


large_file_bytes = NUMBER_OF_LARGE_FILES * BYTES_PER_LARGE_FILE

repo_bytes = NUMBER_OF_CODE_FILES * LINES_PER_CODE_FILE * CHARACTERS_PER_LINE + \
        large_file_bytes + NUMBER_OF_SMALL_FILES * CHARACTERS_PER_SMALL_FILE

large_file_percent = int(large_file_bytes / repo_bytes * 100)

repo_gb = repo_bytes / 1024 / 1024 / 1024

repo_files = NUMBER_OF_CODE_FILES + NUMBER_OF_LARGE_FILES + NUMBER_OF_SMALL_FILES

print()
print(f"Repo will be approximately {repo_bytes:,} bytes ({repo_gb:.3f} GB) spread across {repo_files:,} files.")
print(f"Approximately {large_file_percent}% of the repo is in large files")
print("Generating large repo in %s" % Path.cwd())
if (input("Type YES to proceed --> ") != "YES"):
    sys.exit()

def generate_file(directory, increment, datum_func, binary = False):
    datum = datum_func()
    myfile = Path(f"{directory}/file-{increment:0>8d}")
    if binary:
        myfile.write_bytes(datum)
    else:
        myfile.write_text(datum)

def small_files_datum():
    return ''.join([secrets.choice(string.printable) for _ in range(CHARACTERS_PER_SMALL_FILE)])

def large_files_datum():
    return secrets.token_bytes(BYTES_PER_LARGE_FILE)

def code_files_datum():
    return "\n".join([''.join([secrets.choice(string.printable) for _ in range(CHARACTERS_PER_LINE)]) for i in range(LINES_PER_CODE_FILE)])

sequence = {
        'small_files': (NUMBER_OF_SMALL_FILES, small_files_datum, False),
        'large_files': (NUMBER_OF_LARGE_FILES, large_files_datum, True),
        'code_files': (NUMBER_OF_CODE_FILES, code_files_datum, False)
    }

for datum_type in sequence:
    print()
    time = datetime.now()
    Path(datum_type).mkdir(exist_ok=True)
    files_count = sequence[datum_type][0]
    datum_func = sequence[datum_type][1]
    binary = sequence[datum_type][2]
    print(f"Starting {datum_type} at {time} - {files_count} files to build")
    for ifile in range(files_count):
        generate_file(datum_type, ifile, datum_func, binary)
        sys.stdout.write(".")
        if (not (ifile + 1) % 100):
            sys.stdout.write(f" {datum_type} {ifile + 1}/{files_count} ")
        sys.stdout.flush()

print()
time = datetime.now()
print(f"Finished at {time}")
